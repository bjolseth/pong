
function createColorPicker(onSelect, selected) {
  const bar = document.createElement('div');
  bar.className = 'colors';
  config.colors.forEach(c => {
    const box = document.createElement('div');
    box.classList.add('color-box');
    const color = document.createElement('div');
    color.classList.add('color');
    color.style.backgroundColor = c;
    box.appendChild(color);
    box.ontouchstart = (e) => {
      e.preventDefault();
      onSelect(c);
      const prev = bar.querySelector('.color.selected');
      if (prev) prev.classList.remove('selected');
      color.classList.add('selected');
    }

    if (c === selected) color.classList.add('selected');
    bar.appendChild(box);
  });
  bar.ontouchstart = (e) => e.stopPropagation(); // not a click to draw on canvas etc

  return bar;
}
