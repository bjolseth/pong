const config = {
  colors: ['black', '#ffb300', '#ff1a1b', '#e900d8', '#00c4e8', '#00e258'],
  ballSize: {
   w: 70,
   h: 70,
 },
 startSpeed: 8,
 maxSpeed: 30,
 speedIncreasePerHit: 0.6,
 initYSpeed: {
   min: 1.5,
   max: 3,
 },
 maxBarHeight: 450,
 winScore: 5,
 standbyTime: 30,
 twoColorPickers: true,
};

const classicConfig = {
  colors: ['white'],
  ballSize: {
   w: 20,
   h: 20,
 },
}
